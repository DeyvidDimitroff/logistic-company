CREATE TABLE users (
  `id` INT NOT NULL AUTO_INCREMENT UNIQUE,
  name VARCHAR(50) NOT NULL,
  email VARCHAR(50) NOT NULL,
  password VARCHAR(100) NOT NULL,
  phone_number VARCHAR(10),
  enabled TINYINT NOT NULL DEFAULT 1,
  PRIMARY KEY (email)
);

CREATE TABLE authorities (
  email VARCHAR(50) NOT NULL,
  authority VARCHAR(50) NOT NULL,
  FOREIGN KEY (email) REFERENCES users(email)
);

CREATE UNIQUE INDEX ix_auth_email on authorities (email,authority);

-- User user@email.pass/pass
INSERT INTO users (id, name, email, password, enabled)
  values ('1', 'USER USEROV',
    'user@nbuexpress.com',
    '$2a$10$r1Gm0wN1hZrMXQU7bchXsOI8zxLkmy3UjTOE9MFagcJOYQ7NeFLyu',
    1),
    ('2', 'ADMIN ADMINOV',
    'admin@nbuexpress.com',
    '$2a$10$XqCVaNX06ORWKY6Vm56RNOKx.LgS0xm/VhN3oFOlu/p6K5ifOW08a',
    1);

INSERT INTO authorities (email, authority)
  values ('user@nbuexpress.com', 'ROLE_USER'),
   ('admin@nbuexpress.com', 'ROLE_ADMIN');

CREATE TABLE `logistic-company`.`shipment` (
                                               `id` INT NOT NULL AUTO_INCREMENT,
                                               `sender_id` INT NOT NULL,
                                               `recipient_id` INT NOT NULL,
                                               `office_employee_id` INT NOT NULL,
                                               `courier_id` INT NOT NULL,
                                               `shipping_address` VARCHAR(45) NOT NULL,
                                               `weight` DOUBLE NOT NULL,
                                               `is_sent_to_address` TINYINT NOT NULL DEFAULT 0,
                                               `is_received` TINYINT NOT NULL DEFAULT 0,
                                               PRIMARY KEY (`id`))
    COMMENT = 'id;\nclient_id (user_id);\nrecipeinet_id (user_id);\nOffice_employee_id (user_id);\ncourier_id (user_id);\nString shipping Address;\nweight;\nboolean isSendToAddrss;\nboolean received;';

CREATE TABLE `logistic-company`.`cities` (
                                             `id` INT NOT NULL AUTO_INCREMENT,
                                             `name` VARCHAR(45) NOT NULL,
                                             PRIMARY KEY (`id`))
    COMMENT = 'Table for getting all of the cities easier.\n';

ALTER TABLE `logistic-company`.`cities`
RENAME TO  `logistic-company`.`municipalities` ;

INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('1', 'Blagoevgrad');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('2', 'Burgas');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('3', 'Dobrich');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('4', 'Gabrovo');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('5', 'Haskovo');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('6', 'Kardzhali');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('7', 'Kyustendil');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('8', 'Lovech');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('9', 'Montana');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('10', 'Pazardzhik');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('11', 'Pernik');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('12', 'Pleven');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('13', 'Plovdiv');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('14', 'Razgrad');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('15', 'Ruse');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('16', 'Shumen');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('17', 'Silistra');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('18', 'Sliven');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('19', 'Smolyan');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('20', 'Sofia (province)');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('21', 'Sofia City');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('22', 'Stara Zagora');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('23', 'Targovishte');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('24', 'Varna');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('25', 'Veliko Tarnovo');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('26', 'Vidin');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('27', 'Vratsa');
INSERT INTO `logistic-company`.`municipalities` (`id`, `name`) VALUES ('28', 'Yambol');

CREATE TABLE `logistic-company`.`offices` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `street_address` VARCHAR(45) NOT NULL,
  `city_id` INT NOT NULL,
  `working_time` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
COMMENT = 'Table of offices \nid;\nString name;\ncity_id;';

CREATE TABLE `logistic-company`.`users_shipments` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `shipment_id` INT NOT NULL,
  PRIMARY KEY (`id`))
COMMENT = 'table for relations of users and shipments';

ALTER TABLE `logistic-company`.`users`
DROP COLUMN `phone_number`,
ADD COLUMN `user_details_id` INT NOT NULL AFTER `enabled`;

CREATE TABLE `logistic-company`.`user_details` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `salary` DOUBLE NULL,
  `phone_number` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
COMMENT = 'user_details\nTable containing user details for all of the roles:\nClient - Two sets of shipments - sent and received\nOffice employee - Salary, Shipments processed\nCourier - salary, shimpents processed\nfields:\nid;\nuser_shipment_id;\nsalary; 1courier 2officemp 3sender 4receiver';

INSERT INTO `logistic-company`.`user_details` (`id`, `salary`, `phone_number`) VALUES ('1', '400', '0888888888');
INSERT INTO `logistic-company`.`user_details` (`id`, `salary`) VALUES ('2', '400');
INSERT INTO `logistic-company`.`user_details` (`id`, `phone_number`) VALUES ('3', '0888888881');
INSERT INTO `logistic-company`.`user_details` (`id`, `phone_number`) VALUES ('4', '0888888882');

CREATE TABLE `logistic-company`.`company` (
  `name` VARCHAR(40) NOT NULL,
  PRIMARY KEY (`name`))
COMMENT = 'table for constant company details\n';

INSERT INTO `logistic-company`.`company` (`name`) VALUES ('NBU Express');

INSERT INTO `logistic-company`.`users` (`id`, `name`, `email`, `password`, `enabled`, `user_details_id`) VALUES ('3', 'IZPRASHTACH IZPRASHTACHOV', 'sender@nbuexpress.com', '$2a$10$IeZuvInCnyEhot15wCs9f.G1ZHniolg./hpqdOtGZ4vvX4ckcNhkC', '1', '4');
INSERT INTO `logistic-company`.`users` (`id`, `name`, `email`, `password`, `enabled`, `user_details_id`) VALUES ('4','POLUCHATEL POLUCHATELOV', 'recipient@nbuexpress.com', '$2a$10$6wd1l1Ooec3iPV7HhgboCOPoPcwmHTSueZiiHvL/hVblHPUEDaFrC', '1', '3');
INSERT INTO `logistic-company`.`users` (`id`, `name`, `email`, `password`, `enabled`, `user_details_id`) VALUES ('5','OFICAR OFICAROV', 'office@nbuexpress.com', '$2a$10$K7PI9ytBQCWjavtH3btBFO4R685fBkzBPi9rJ3OxzMTbOUGVRlgGe', '1', '2');
INSERT INTO `logistic-company`.`users` (`id`, `name`, `email`, `password`, `enabled`, `user_details_id`) VALUES ('6','KURIER KURIEROV', 'courier@nbuexpress.com', '$2a$10$S95DBBedzxOVUNfo5zFUn.W0ZmbAnlfHEeWSl.r4uiT3Gnn1p5ecG', '1', '1');
UPDATE `logistic-company`.`users` SET `user_details_id` = '0' WHERE (`email` = 'admin@nbuexpress.com');
UPDATE `logistic-company`.`users` SET `user_details_id` = '0' WHERE (`email` = 'user@nbuexpress.com');

ALTER TABLE `logistic-company`.`shipment`
CHANGE COLUMN `sender_id` `sender_id` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `recipient_id` `recipient_id` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `office_employee_id` `office_employee_id` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `courier_id` `courier_id` VARCHAR(25) NOT NULL ;

ALTER TABLE `logistic-company`.`shipment`
CHANGE COLUMN `sender_id` `sender_email` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `recipient_id` `recipient_email` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `office_employee_id` `office_employee_email` VARCHAR(25) NOT NULL ,
CHANGE COLUMN `courier_id` `courier_email` VARCHAR(25) NOT NULL ;

ALTER TABLE `logistic-company`.`shipment`
CHANGE COLUMN `sender_email` `sender_email` VARCHAR(25) NULL ,
CHANGE COLUMN `recipient_email` `recipient_email` VARCHAR(25) NULL ,
CHANGE COLUMN `office_employee_email` `office_employee_email` VARCHAR(25) NULL ,
CHANGE COLUMN `courier_email` `courier_email` VARCHAR(25) NULL ;

INSERT INTO `logistic-company`.`shipment` (`id`, `sender_email`, `recipient_email`, `office_employee_email`, `courier_email`, `shipping_address`, `weight`, `is_sent_to_address`, `is_received`) VALUES ('2', 'sender@nbuexpress.com', 'recipient@nbuexpress.com', 'office@nbuexpress.com', 'courier@nbuexpress.com', 'ADRES ZA DOSTAVKA 2', '22.22', '1', '1');
INSERT INTO `logistic-company`.`shipment` (`id`, `sender_email`, `recipient_email`, `office_employee_email`, `courier_email`, `shipping_address`, `weight`, `is_sent_to_address`, `is_received`) VALUES ('3', 'sender@nbuexpress.com', 'recipient@nbuexpress.com', 'office@nbuexpress.com', 'courier@nbuexpress.com', 'ADRES ZA DOSTAVKA 3', '33.33', '1', '0');
INSERT INTO `logistic-company`.`shipment` (`id`, `sender_email`, `recipient_email`, `courier_email`, `shipping_address`, `weight`, `is_sent_to_address`) VALUES ('1', 'sender@nbuexpress.com', 'recipient@nbuexpress.com', 'courier@nbuexpress.com', 'ADRES ZA DOSTAVKA 1', '11.11', '1');

CREATE TABLE `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `logistic-company`.`user_types` (`id`, `name`) VALUES ('1', 'SENDER');
INSERT INTO `logistic-company`.`user_types` (`id`, `name`) VALUES ('2', 'RECIPIENT');
INSERT INTO `logistic-company`.`user_types` (`id`, `name`) VALUES ('3', 'OFFICE');
INSERT INTO `logistic-company`.`user_types` (`id`, `name`) VALUES ('4', 'COURIER');

ALTER TABLE `logistic-company`.`users_shipments`
ADD COLUMN `user_type_id` INT NOT NULL AFTER `shipment_id`;

INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('1', '3', '2', '1');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('2', '4', '2', '2');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('3', '5', '2', '3');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('4', '6', '2', '4');

INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('5', '3', '1', '1');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('6', '4', '1', '2');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('7', '5', '1', '3');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('8', '6', '1', '4');

INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('9', '3', '3', '1');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('10', '4', '3', '2');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('11', '5', '3', '3');
INSERT INTO `logistic-company`.`users_shipments` (`id`, `user_id`, `shipment_id`, `user_type_id`) VALUES ('12', '6', '3', '4');

ALTER TABLE `logistic-company`.`shipment`
DROP COLUMN `courier_email`,
DROP COLUMN `office_employee_email`,
DROP COLUMN `recipient_email`,
DROP COLUMN `sender_email`;

ALTER TABLE `logistic-company`.`shipment`
ADD COLUMN `price` DOUBLE NOT NULL DEFAULT 0.00 AFTER `weight`;

UPDATE `logistic-company`.`shipment` SET `price` = '11.11' WHERE (`id` = '1');
UPDATE `logistic-company`.`shipment` SET `price` = '22.22' WHERE (`id` = '2');
UPDATE `logistic-company`.`shipment` SET `price` = '33.33' WHERE (`id` = '3');

INSERT INTO `logistic-company`.`offices` (`id`, `street_address`, `city_id`, `working_time`) VALUES ('1', 'Street1', '1', '9-18');
INSERT INTO `logistic-company`.`offices` (`id`, `street_address`, `city_id`, `working_time`) VALUES ('2', 'Street2', '2', '10-17');
INSERT INTO `logistic-company`.`offices` (`id`, `street_address`, `city_id`, `working_time`) VALUES ('3', 'Street3', '3', '9-18');

INSERT INTO `logistic-company`.`authorities` (email, authority)
values ('recipient@nbuexpress.com', 'ROLE_USER'),
    ('sender@nbuexpress.com', 'ROLE_USER'),
    ('office@nbuexpress.com', 'ROLE_USER'),
    ('courier@nbuexpress.com', 'ROLE_USER');