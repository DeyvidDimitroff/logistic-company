$(document).ready(function(){
	$('.shipmentT .status').add('.profileT .status').each(function(){
		if($(this).html() == 'false') {
			$(this).html('<img class="notaccepted tableicon" />');
		}
		if($(this).html() == 'true') {
			$(this).html('<img class="accepted tableicon" />');
		}
	});
	$('.approvalT .employeename').each(function(){
		if(!$(this).html() == '') {
			$(this).parent().hide();
		}
	});
	$('.toBeDeliveredT .deliverstatus').each(function(){
		if($(this).html() == 'true') {
			$(this).parent().hide();
		}
	});
	$('.toBeDeliveredT .deliverytype').each(function(){
		if($(this).find('.office')) {
			$(this).parent().hide();
		}
	});
	$('.shipmentformregister').click(function(){
		$('.shipmentregister').fadeToggle();
		$('.shipmentregister').css('display', 'inline-block');
	});
	$('.switch input').each(function(){
		if($(this).attr('value') == 'true') {
			$(this).prop('checked', true);
			$(this).parent().children('a').html("TURN OFF");
		}
		else {
			$(this).prop('checked', false);
			$(this).parent().children('a').html("TURN ON");
		}
	});
	// $('.switch input').on('change', function() {
	// 	if($(this).is(':checked')) {
	// 		$(this).prop('value', 'true');
	// 		$(this).parent().children('a').html("TURN OFF");
	// 	}
	// 	else {
	// 		$(this).prop('value', 'false');
	// 		$(this).parent().children('a').html("TURN ON");
	// 	}
	// });
	$('.shipmentT .deliverytype').add('.profileT .deliverytype').each(function(){
		if($(this).html() == 'false') {
			$(this).html('<img class="office tableicon" />');
		}
		if($(this).html() == 'true') {
			$(this).html('<img class="address tableicon" />');
		}
	});
	$('.burger-menu').click(function(){
		$('.navbg').slideToggle();
	});
	$('.loginform p a').click(function(){
		$('.registerform').fadeToggle();
		$('.loginform').fadeToggle();
	});

	$('.registerform p a').click(function(){
		$('.registerform').fadeToggle();
		$('.loginform').fadeToggle();
	});
	$('.reference-title').click(function(){
		$('.reference-nav form').hide();
		var i = $('.reference-title').index($(this));
		var s = $('.reference-info').length;
		if($('.reference-info').eq(i).is(":visible")) {
			$('.arrow').eq(i).css({'transform': 'none'});
		}
		else {
			$('.arrow').eq(i).css({'transform': 'rotate(-180deg)',
				'transition-duration': '.5s'});
		}
		$('.reference-info').eq(i).slideToggle();
		for ( var z = 0; z < i; z++ ) {
			if($('.reference-info').eq(z).is(":visible")) {
				$('.reference-info').eq(z).slideToggle();
				$('.arrow').eq(z).css({'transform': 'none'});
			}
		}
		for ( var y = i+1 ; y < s+1; y++) {
			if($('.reference-info').eq(y).is(":visible")) {
				$('.reference-info').eq(y).slideToggle();
				$('.arrow').eq(y).css({'transform': 'none'});
			}
		}
	});
	$('.registerShipment').click(function(){
		$('.shipmentreg')[0].reset();
		$('.deliverytype').children().eq(0).attr('selected', 'true');
		$('.shipmentreg').fadeIn().css("display","inline-block");
	});
	$('.registerWorker').click(function(){
		$('.workerreg')[0].reset();
		$('.reference-nav form').hide();
		$('.workerreg').fadeIn().css("display", "inline-block");
	});
	$('.registerOffice').click(function(){
		$('.officereg')[0].reset();
		$('.reference-nav form').hide();
		$('.officereg').fadeIn().css("display", "inline-block");
	});
	$('td.delete').click(function(){
		$(this).closest('tr').remove();
	});
	$('.officeT td.edit').click(function(){
		$('.reference-nav form').hide();
		$('.officeedit').fadeIn(500).css('display', 'inline-block');

		$('input[name=city]').val($(this).closest('tr').children().html());
		$('input[name=officeaddress]').val($(this).closest('tr').children().eq(1).html());
		$('input[name=worktime]').val($(this).closest('tr').children().eq(2).html());
	});
	$('.personalT td.edit').click(function(){
		$('.reference-nav form').hide();
		$('.workeredit').fadeIn(500).css('display', 'inline-block');
		$('input[name=name]').val($(this).closest('tr').children().html());
		$('input[name=email]').val($(this).closest('tr').children().eq(1).html());
		$('input[name=newsalary]').val($(this).closest('tr').children().eq(2).html());
		$('select[name=roles]').val($(this).closest('tr').children().eq(3).html());
	});
	$('.shipmentT td.edit').click(function(){
		$('.addressPick').hide();
		$('.officeList').hide();
		$('.reference-nav form').hide();
		$('.shipmentedit').fadeIn(500).css('display', 'inline-block');
		$('input[name=sentby]').val($(this).closest('tr').children().html());
		$('input[name=sentto]').val($(this).closest('tr').children().eq(1).html());
		$('input[name=weight]').val($(this).closest('tr').children().eq(2).html());
		$('.addressPick input[type=text]').val($(this).closest('tr').children().eq(3).html());
		if($(this).closest('tr').children().eq(4).children().hasClass('address')) {
			$('.deliverytype option[value=address]').attr('selected', 'selected');
			$('.addressPick').css('display', 'block');
		}
		else if($(this).closest('tr').children().eq(4).children().hasClass('office')) {
			$('.deliverytype option[value=office]').attr('selected', 'selected');
			$('.officeList').css('display', 'block');
		}
		if($(this).closest('tr').children().eq(5).children().hasClass('notaccepted')) {
			$('select[for=status] option[value=notaccepted]').attr('selected', 'selected');
		}
		else if($(this).closest('tr').children().eq(5).children().hasClass('accepted')) {
			$('select[for=status] option[value=accepted]').attr('selected', 'selected');
		}
	});
	$('.clientsT td.edit').click(function(){
		$('.reference-nav form').hide();
		$('.clientedit').fadeIn(500).css('display', 'inline-block');
		$('input[name=name]').val($(this).closest('tr').children().html());
		$('input[name=email]').val($(this).closest('tr').children().eq(1).html());
		$('input[name=phone]').val($(this).closest('tr').children().eq(2).text());
		$('input[name=orders]').val($(this).closest('tr').children().eq(3).children().html());
	});
	$('.logoutalert span').click(function(){
		$(this).parent().fadeToggle();
	});
	if ($(".logoutalert").is(':visible')) {
		setTimeout(function(){
			$('.logoutalert').slideToggle();
		}, 5000);
	}
	if ($(".wronglogin").is(':visible')) {
		$('.wronglogin').effect('shake', {distance:3});
	}
	$('select[value=shipmentStatus]').on('change', function(){
		if($('option[value=onlyaccepted]').is(":selected")) {
			$('.resetfilters').css('display', 'inline-block');
			$('.shipmentT tr').each(function() {
				if($(this).find(".notaccepted").length > 0) {
					$(this).hide();
				}
				if($(this).find(".accepted").length > 0) {
					$(this).show();
				}
			});
		}
		if($('option[value=onlynotaccepted]').is(":selected")) {
			$('.resetfilters').css('display', 'inline-block');
			$('.shipmentT tr').each(function() {
				if($(this).find(".accepted").length > 0) {
					$(this).hide();
				}
				if($(this).find(".notaccepted").length > 0) {
					$(this).show();
				}
			});
		}
	});
	$('.sort select').on('change', function(){
		$('.resetfilters').css('display', 'block');
	});
	$('.resetfilters').click(function(){
		$('.sort select').each(function(){
			$(this).children().eq(0).prop("selected", "true");
		});
		$('.shipmentT tr').each(function() {
			if($(this).find(".notaccepted").length > 0) {
				$(this).show();
			}
			if($(this).find(".accepted").length > 0) {
				$(this).show();
			}
		});
		$(this).hide();
	});
	$('.deliverytype').on('change', function(){
		if($('.deliverytype option[value=office]').is(":selected")) {
			$('.officeList').css('display', 'block');
			$('.addressPick').css('display', 'none');
		}
		if($('.deliverytype option[value=address]').is(":selected")) {
			$('.addressPick').css('display', 'block');
			$('.officeList').css('display', 'none');
		}
	});
	$('.newdeliverytype').on('change', function(){
		if($('.newdeliverytype option[value=office]').is(":selected")) {
			$('.officeList').css('display', 'block');
			$('.addressPick').css('display', 'none');
		}
		if($('.newdeliverytype option[value=address]').is(":selected")) {
			$('.shipmentedit .addressPick').css('display', 'block');
			$('.shipmentedit .officeList').css('display', 'none');
		}
	});
	$("select[value=sentby]").on('change', function(){
		var selected = $(this).children("option:selected").val();
		$('.shipmentT tr').each(function() {
			if(!$(this).find(selected)) {
				$(this).hide();
			}
		});
	});
});

function submitForm() {
	$('.calculatorform').submit(function(e){
		e.preventDefault();
	});
	var priceTag = 0;
	if($("input[value='office']").is(":checked")) {
		priceTag = $("input[name='weight']").val()*2.14 + 3;
		$('.pricetag').html('The price for the delivery will be <b>$' + priceTag.toFixed(2) + "</b></p>");
	}
	else if($("input[value='address']").is(":checked")) {
		priceTag = $("input[name='weight']").val()*2.3 + 6;

		$('.pricetag').html('The price for the delivery will be <b>$' + priceTag.toFixed(2) + "</b></p>");
	}
	else {
		$('.pricetag').html('Please add weight and choose delivery type!');
	}
	if ($('.pricetag').is(":hidden")) {
		$(".pricetag").fadeToggle();
	}
	$('.calculatorform')[0].reset();
}

function register() {
	$('.shipmentreg').submit(function(e){
		e.preventDefault();
	});
	$('.shipmentreg')[0].reset();
	$('.workerreg')[0].reset();
	$('.officereg')[0].reset();
	$('.officeedit')[0].reset();
	$('.clientedit')[0].reset();
}

function edit() {
	$('.shipmentreg').submit(function(e){
		e.preventDefault();
	});
	$('.shipmentreg')[0].reset();
	$('.workerreg')[0].reset();
	$('.officereg')[0].reset();
	$('.officeedit')[0].reset();
	$('.clientedit')[0].reset();
}