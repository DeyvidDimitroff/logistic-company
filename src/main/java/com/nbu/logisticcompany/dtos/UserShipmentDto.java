package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.UserShipment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserShipmentDto {
    private UserDto userDto;
    private String userType;
    private ShipmentDto shipmentDto;

    public static UserShipmentDto convert(UserShipment userShipment){
        UserShipmentDto userShipmentDto = new UserShipmentDto();
        userShipmentDto.setUserDto(UserDto.convert(userShipment.getUser()));
        userShipmentDto.setUserType(userShipment.getUserTypes().getName());
        userShipmentDto.setShipmentDto(ShipmentDto.convert(userShipment.getShipment()));

        return userShipmentDto;
    }

    public static List<UserShipmentDto> convertList(List<UserShipment> userShipmentList){
        List<UserShipmentDto> userShipmentDtoList = new ArrayList<>();
        for (UserShipment userShipment : userShipmentList) {
            userShipmentDtoList.add(UserShipmentDto.convert(userShipment));
        }

        return userShipmentDtoList;
    }
}
