package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Municipality;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MunicipalityDto {
    String name;

    public static MunicipalityDto fromObject (Municipality municipality) {
        MunicipalityDto municipalityDto = new MunicipalityDto();
        municipalityDto.setName(municipality.getName());

        return municipalityDto;
    }
}
