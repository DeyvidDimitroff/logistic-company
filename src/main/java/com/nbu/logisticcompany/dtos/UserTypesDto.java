package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Shipment;
import com.nbu.logisticcompany.models.UserTypes;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserTypesDto {
    private String name;

    public static UserTypesDto convert(UserTypes userTypes){
        UserTypesDto userTypesDto = new UserTypesDto();

        userTypesDto.setName(userTypes.getName());
        return userTypesDto;
    }

    public static List<UserTypesDto> convertList(List<UserTypes> userTypesList){
        List<UserTypesDto> userTypesDtoList = new ArrayList<>();
        for (UserTypes userType : userTypesList) {
            userTypesDtoList.add(UserTypesDto.convert(userType));
        }

        return userTypesDtoList;
    }
}
