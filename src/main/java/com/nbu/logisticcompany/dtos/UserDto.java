package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Shipment;
import com.nbu.logisticcompany.models.User;
import com.nbu.logisticcompany.models.UserShipment;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Integer id;
    private String name;
    private String email;
    private Double salary;
    private String role;
    private String phoneNumber;
    private Boolean isEnabled;

    public static UserDto convert(User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setEmail(user.getEmail());
        userDto.setName(user.getName());
        if (user.getUserDetails() != null && user.getUserDetails().getPhoneNumber() != null) {
            userDto.setPhoneNumber(user.getUserDetails().getPhoneNumber());
        }

        if (user.getUserDetails() != null && user.getUserDetails().getSalary() != null) {
            userDto.setSalary(user.getUserDetails().getSalary());
        }

        if (user.getShipments() != null && user.getShipments().size() > 0) {
            for (UserShipment userShipment : user.getShipments()) {
                if (userDto.getRole() == null) {
                    userDto.setRole(userShipment.getUserTypes().getName());
                } else {
                    if (!userDto.getRole().contains(userShipment.getUserTypes().getName())) {
                        userDto.setRole(userDto.getRole() + ", " + userShipment.getUserTypes().getName());
                    }
                }
            }
        }
        userDto.setIsEnabled(user.isEnabled());

        return userDto;
    }

    public static List<UserDto> convertList(List<User> users) {
        ArrayList<UserDto> userDtos = new ArrayList<>();

        for (User user : users) {
            userDtos.add(UserDto.convert(user));
        }
        return userDtos;
    }

}
