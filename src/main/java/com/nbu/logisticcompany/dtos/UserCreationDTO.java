package com.nbu.logisticcompany.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserCreationDTO {
    public String name;
    public String email;
    public String password;
    public String phoneNumber;
    private Double salary;
    private String role;
}
