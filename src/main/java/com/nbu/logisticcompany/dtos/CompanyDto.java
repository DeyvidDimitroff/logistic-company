package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Company;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyDto {
    private String name;

    public static CompanyDto fromObject(Company company) {
        CompanyDto companyDto = new CompanyDto();
        companyDto.setName(company.getName());

        return companyDto;
    }
}
