package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Office;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OfficeDto {
    Integer id;
    String address;
    String city;
    String workingTime;

    public static OfficeDto convert (Office office) {
        OfficeDto officeDto = new OfficeDto();
        officeDto.setId(office.getId());
        officeDto.setAddress(office.getStreetAddress());
        officeDto.setCity(office.getCity().getName());
        officeDto.setWorkingTime(office.getWorkingTime());

        return officeDto;
    }

    public static List<OfficeDto> convertList (List<Office> officeList) {
        List<OfficeDto> offices = new ArrayList<>();

        for (Office office : officeList) {
            offices.add(OfficeDto.convert(office));
        }

        return offices;
    }
}
