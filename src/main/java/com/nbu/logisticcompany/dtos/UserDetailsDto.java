package com.nbu.logisticcompany.dtos;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UserDetailsDto {
    Integer id;
    Double salary;
    String phoneNumber;


}
