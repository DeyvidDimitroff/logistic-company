package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Municipality;
import com.nbu.logisticcompany.models.Shipment;
import com.nbu.logisticcompany.models.UserShipment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentDto {
    private Integer id;
    private String senderName;
    private String recipientName;
    private String officeEmployeeName;
    private String courierName;
    private String shippingAddress;
    private Double weight;
    private Double price;
    private Boolean isSentToAddress;
    private Boolean isReceived;

    public static ShipmentDto convert(Shipment shipment) {
        ShipmentDto shipmentDto = new ShipmentDto();
        shipmentDto.setId(shipment.getId());
        for (UserShipment us : shipment.getUserShipments()) {
            if (us.getUserTypes().getId() == 1) {
                shipmentDto.setSenderName(us.getUser().getName());
            }
            if (us.getUserTypes().getId() == 2) {
                shipmentDto.setRecipientName(us.getUser().getName());
            }
            if (us.getUserTypes().getId() == 3) {
                shipmentDto.setOfficeEmployeeName(us.getUser().getName());
            }
            if (us.getUserTypes().getId() == 4) {
                shipmentDto.setCourierName(us.getUser().getName());
            }
        }
        shipmentDto.setPrice(shipment.getPrice());
        shipmentDto.setShippingAddress(shipment.getShippingAddress());
        shipmentDto.setWeight(shipment.getWeight());
        shipmentDto.setIsSentToAddress(shipment.getIsSentToAddress());
        shipmentDto.setIsReceived(shipment.getIsReceived());

        return shipmentDto;
    }

    public static List<ShipmentDto> convertList(List<Shipment> shipments) {
        ArrayList<ShipmentDto> shipmentDtos = new ArrayList<>();

        for (Shipment s : shipments) {
            shipmentDtos.add(ShipmentDto.convert(s));
        }
        return shipmentDtos;
    }
}
