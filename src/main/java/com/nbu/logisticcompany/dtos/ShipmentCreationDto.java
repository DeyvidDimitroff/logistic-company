package com.nbu.logisticcompany.dtos;

import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.models.UserShipment;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ShipmentCreationDto {
    private String senderName;
    private String recipientName;
    private String shippingAddress;
    private Integer office;
    private Double weight;
    private Double price;
    private String isSentToAddress;
    List<UserShipmentDto> userShipments;

}
