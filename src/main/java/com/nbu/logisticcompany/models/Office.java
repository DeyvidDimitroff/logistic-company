package com.nbu.logisticcompany.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "offices")
public class Office {
    @Id
    @Column(unique = true, name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Size(max = 45)
    @Column(unique = true, name = "street_address")
    private String streetAddress;

    @ManyToOne
    @JoinColumn(name = "city_id")
    private Municipality city;

    @Column(name = "working_time")
    private String workingTime;
}
