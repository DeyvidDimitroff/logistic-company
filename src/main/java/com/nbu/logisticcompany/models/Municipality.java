package com.nbu.logisticcompany.models;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "municipalities")
public class Municipality {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @Size(max = 45)
    @Column(unique = true, name = "name")
    private String name;
}
