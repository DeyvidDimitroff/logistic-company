package com.nbu.logisticcompany.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "shipment")
public class Shipment {
    @Id
    @Column(unique = true, name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "shipping_address")
    private String shippingAddress;

    @Column(name = "weight")
    private Double weight;

    @Column(name = "price")
    private Double price;

    @Column(name = "is_sent_to_address")
    private Boolean isSentToAddress;

    @Column(name = "is_received")
    private Boolean isReceived;

    @OneToMany(mappedBy = "shipment", fetch = FetchType.EAGER)
    private List<UserShipment> userShipments;
}
