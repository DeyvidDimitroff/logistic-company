package com.nbu.logisticcompany.models;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Entity
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user_types")
public class UserTypes {
    @Id
    @Column(unique = true, name = "id")
    private Integer id;

    @Size(max = 10)
    @Column(unique = true, name = "name")
    private String name;
}
