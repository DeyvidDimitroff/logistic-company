package com.nbu.logisticcompany.controllers;

import com.nbu.logisticcompany.dtos.ShipmentCreationDto;
import com.nbu.logisticcompany.dtos.ShipmentDto;
import com.nbu.logisticcompany.services.ShipmentService;
import com.nbu.logisticcompany.services.UserShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ShipmentController {
    private ShipmentService shipmentService;
    private UserShipmentService userShipmentService;

    @Autowired
    public ShipmentController(ShipmentService shipmentService, UserShipmentService userShipmentService) {
        this.shipmentService = shipmentService;
        this.userShipmentService = userShipmentService;
    }

    @GetMapping("/shipments")
    public List<ShipmentDto> showShipments() {
        return shipmentService.getAll();
    }

    @PostMapping("/create-shipment")
    public String registerEmployee(@ModelAttribute ShipmentCreationDto shipment, Authentication authentication) {
        shipment.setSenderName(authentication.getName());
        shipmentService.create(shipment);
        return "index";
    }

    @GetMapping("/approve-shipment")
    public String approveShipment(@RequestParam(name="shipmentId")Integer shipmentId, Authentication authentication) {
        userShipmentService.approve(shipmentId, authentication.getName());

        return "index";
    }

    @GetMapping("/deliver-shipment")
    public String deliverShipment(@RequestParam(name="shipmentId")Integer shipmentId, Authentication authentication) {
        userShipmentService.deliver(shipmentId, authentication.getName());

        return "index";
    }

}
