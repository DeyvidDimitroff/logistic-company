package com.nbu.logisticcompany.controllers;

import com.nbu.logisticcompany.dtos.OfficeDto;
import com.nbu.logisticcompany.dtos.ShipmentCreationDto;
import com.nbu.logisticcompany.dtos.ShipmentDto;
import com.nbu.logisticcompany.dtos.UserCreationDTO;
import com.nbu.logisticcompany.services.OfficeService;
import com.nbu.logisticcompany.services.UserService;
import com.nbu.logisticcompany.services.UserShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HomeController {
    private UserShipmentService userShipmentService;
    private UserService userService;
    private OfficeService officeService;

    @Autowired
    public HomeController(UserShipmentService userShipmentService, UserService userService, OfficeService officeService) {
        this.userShipmentService = userShipmentService;
        this.userService = userService;
        this.officeService = officeService;
    }

    @GetMapping("/")
    public String showHomePage(Model model) {
        loadShipmentProperties(model);
        loadOfficesInModel(model);
        return "index";
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }

    @GetMapping("/login")
    public String showLogin(Model model) {
        model.addAttribute("user", new UserCreationDTO());
        return "login";
    }

    @GetMapping("/offices")
    public String showOfficesPage() {
        return "offices";
    }

    @GetMapping("/aboutus")
    public String showAboutUsPage() {
        return "aboutus";
    }

    @GetMapping("/calculate")
    public String showCalculatePage() {
        return "calculate";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

    @GetMapping("/profile")
    public String showProfile(Model model, Authentication authentication) {
        loadShipmentsByUser(model, authentication);
        return "profile";
    }

    @GetMapping("/delete-user")
    public String deleteUser(@RequestParam(name="userId")Integer userId) {
        userService.enableOrDisableUser(userId);

        return "redirect:/references";
    }

    private void loadShipmentsByUser(Model model, Authentication authentication) {
        List<ShipmentDto> shipments = userShipmentService.getShipmentsForUser(authentication.getName());
        model.addAttribute("shipments", shipments);
    }

    private void loadShipmentProperties(Model model) {
        ShipmentCreationDto shipment = new ShipmentCreationDto();
        model.addAttribute("shipment", shipment);
    }

    private void loadOfficesInModel(Model model) {
        List<OfficeDto> offices = officeService.getAll();
        model.addAttribute("offices", offices);
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute UserCreationDTO user) {
        userService.createClient(user);
        return "index";
    }

    @PostMapping("/create-employee")
    public String registerEmployee(@ModelAttribute UserCreationDTO user) {
        userService.createEmployee(user);
        return "index";
    }
}


