package com.nbu.logisticcompany.controllers;

import com.nbu.logisticcompany.dtos.*;
import com.nbu.logisticcompany.exceptions.InvalidUserInputException;
import com.nbu.logisticcompany.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class ReferencesController {
    private MunicipalityService municipalityService;
    private ShipmentService shipmentService;
    private UserTypesService userTypesService;
    private UserShipmentService userShipmentService;
    private OfficeService officeService;

    @Autowired
    public ReferencesController(MunicipalityService municipalityService, ShipmentService shipmentService,
                                UserTypesService userTypesService, UserShipmentService userShipmentService,
                                OfficeService officeService) {
        this.municipalityService = municipalityService;
        this.shipmentService = shipmentService;
        this.userTypesService = userTypesService;
        this.userShipmentService = userShipmentService;
        this.officeService = officeService;
    }

    @GetMapping("/references")
    public String showReferences(Model model) {
        List<ShipmentDto> allShipments = shipmentService.getAll();

        model.addAttribute("allShipments", allShipments);
        model.addAttribute("office", new OfficeDto());
        loadMunicipalitiesInModel(model);
        loadUserTypesInModel(model);
        loadOfficesInModel(model);
        loadAllEmployeesInModel(model);
        loadAllClientsInModel(model);
        loadAllSenders(model);
        loadAllReceivers(model);
        return "references";
    }

    @PostMapping("/create-office")
    public String createOffice(@ModelAttribute OfficeDto office, Model model) {
        try {
            officeService.create(office);
        } catch (InvalidUserInputException e) {
            model.addAttribute("office", office);
        }

        return "redirect:/references";
    }

    @GetMapping("/delete-office")
    public String deleteOffice(@RequestParam(name="officeId")Integer officeId) {
        officeService.delete(officeId);

        return "redirect:/references";
    }

    private void loadMunicipalitiesInModel(Model model) {
        List<MunicipalityDto> municipalities = municipalityService.getAll();
        model.addAttribute("municipalities", municipalities);
    }

    private void loadUserTypesInModel(Model model) {
        List<UserTypesDto> userTypes = userTypesService.getAll();
        model.addAttribute("userTypes", userTypes);
    }

    private void loadOfficesInModel(Model model) {
        List<OfficeDto> offices = officeService.getAll();
        model.addAttribute("offices", offices);
    }

    private void loadAllEmployeesInModel(Model model) {
        List<UserDto> allEmployees = userShipmentService.getAllEmployees();
        model.addAttribute("allEmployees", allEmployees);
    }

    private void loadAllClientsInModel(Model model) {
        List<UserDto> allClients = userShipmentService.getAllClients();
        model.addAttribute("allClients", allClients);
    }

    private void loadAllSenders(Model model) {
        List<UserDto> allSenders = userShipmentService.getAllSenders();
        model.addAttribute("allSenders", allSenders);
    }

    private void loadAllReceivers(Model model) {
        List<UserDto> allReceivers = userShipmentService.getAllReceivers();
        model.addAttribute("allReceivers", allReceivers);
    }
}
