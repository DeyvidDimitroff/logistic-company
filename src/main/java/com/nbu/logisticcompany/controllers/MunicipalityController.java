package com.nbu.logisticcompany.controllers;

import com.nbu.logisticcompany.dtos.MunicipalityDto;
import com.nbu.logisticcompany.services.MunicipalityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MunicipalityController {

    MunicipalityService mc;

    @Autowired
    public MunicipalityController(MunicipalityService mc) {
        this.mc = mc;
    }

    @GetMapping("/municipality")
    public List<MunicipalityDto> showMunicipalities() {
        return mc.getAll();
    }
}
