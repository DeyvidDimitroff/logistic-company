package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.models.Shipment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ShipmentRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public ShipmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Shipment> getAll() {
        List<Shipment> shipments;

        try (Session session = sessionFactory.openSession()) {
            shipments = session.createQuery("SELECT s FROM Shipment s", Shipment.class).list();
        }

        return shipments;
    }

    public Shipment getById(int id) {
        Shipment shipment;

        try (Session session = sessionFactory.openSession()) {
            shipment = session.get(Shipment.class, id);
        }

        return shipment;
    }

    public void create(Shipment shipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(shipment);
            session.getTransaction().commit();
        }
    }
}
