package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.models.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class OfficeRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public OfficeRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Office> getAll() {
        List<Office> offices;

        try (Session session = sessionFactory.openSession()) {
            offices = session.createQuery("SELECT o FROM Office o", Office.class).list();
        }

        return offices;
    }

    public void create(Office office) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(office);
            session.getTransaction().commit();
        }
    }
    public Office delete(Office office) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(office);
            transaction.commit();
        } catch (HibernateException he) {
            if (transaction != null) {
                transaction.rollback();
            }
        }

        return office;
    }

    public Office getById(Integer officeId) {
        Office office;

        try (Session session = sessionFactory.openSession()) {
            office = session.get(Office.class, officeId);
        }

        return office;
    }
}
