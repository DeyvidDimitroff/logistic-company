package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.User;
import org.hibernate.*;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    
    public List<User> getAll() {
        List<User> users;

        try (Session session = sessionFactory.openSession()) {
            users = session.createQuery("FROM User", User.class).list();
        }

        return users;
    }

    
    public void create(User user) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.save(user);
            transaction.commit();
        }
    }

    public User getById(int id) {
        User user;

        try (Session session = sessionFactory.openSession()) {
            user = session.get(User.class, id);
        }

        return user;
    }

    public void delete(User user) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.delete(user);
            transaction.commit();
        } catch (HibernateException he) {
            if (transaction != null) {
                transaction.rollback();
            }
            throw he;
        }
    }

    public void update(User user) {
        Transaction transaction = null;
        try (Session session = sessionFactory.openSession()) {
            transaction = session.beginTransaction();
            session.update(user);
            transaction.commit();
        }catch (HibernateException he){
            if (transaction != null) {
                transaction.rollback();
            }
            throw he;
        }
    }

    public User getUserByUserName(String email) {
        User user;

        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Query<User> query = session.createQuery("FROM User u where u.email=:email", User.class);
            query.setParameter("email", email);
            user = query.uniqueResult();
            session.getTransaction().commit();
        }

        return user;
    }
}
