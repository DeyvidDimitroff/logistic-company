package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.Shipment;
import com.nbu.logisticcompany.models.User;
import com.nbu.logisticcompany.models.UserShipment;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserShipmentRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserShipmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<UserShipment> getAll() {
        List<UserShipment> userShipments;

        try (Session session = sessionFactory.openSession()) {
            userShipments = session.createQuery("SELECT us FROM UserShipment us", UserShipment.class).list();
        }

        return userShipments;
    }

    public List<User> getAllEmployees() {
        List<User> users;

        try (Session session = sessionFactory.openSession()) {
            users = session.createQuery("SELECT DISTINCT us.user FROM UserShipment us " +
                    "WHERE us.userTypes.id = 3 OR us.userTypes.id = 4", User.class).list();
        }

        return users;
    }

    public List<User> getAllClients() {
        List<User> users;

        try (Session session = sessionFactory.openSession()) {
            users = session.createQuery("SELECT DISTINCT us.user FROM UserShipment us " +
                    "WHERE us.userTypes.id = 1 OR us.userTypes.id = 2", User.class).list();
        }

        return users;
    }

    public List<User> getAllSenders() {
        List<User> users;

        try (Session session = sessionFactory.openSession()) {
            users = session.createQuery("SELECT DISTINCT us.user FROM UserShipment us " +
                    "WHERE us.userTypes.id = 1", User.class).list();
        }

        return users;
    }

    public List<User> getAllReceivers() {
        List<User> receivers;

        try (Session session = sessionFactory.openSession()) {
            receivers = session.createQuery("SELECT DISTINCT us.user FROM UserShipment us " +
                    "WHERE us.userTypes.id = 2", User.class).list();
        }

        return receivers;
    }

    public List<Shipment> getShipmentsByUser(String email) {
        List<Shipment> receivers;

        try (Session session = sessionFactory.openSession()) {
            receivers = session.createQuery("SELECT DISTINCT us.shipment FROM UserShipment us " +
                    "WHERE us.user.email = :email", Shipment.class)
                    .setParameter("email", email)
                    .list();
        }

        return receivers;
    }

    public void create(UserShipment userShipment) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(userShipment);
            session.getTransaction().commit();
        }
    }
}
