package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.configurations.SessionFactoryUtil;
import com.nbu.logisticcompany.models.Municipality;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public class MunicipalityRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public MunicipalityRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<Municipality> getAll() {
        List<Municipality> municipalities;

        try (Session session = sessionFactory.openSession()) {
            municipalities = session.createQuery("SELECT m FROM Municipality m", Municipality.class).list();
        }

        return municipalities;
    }

    public Municipality getById(int id) {
        return null;
    }

    public Municipality getMunicipalityByName(String municipalityName) {
        Municipality municipality;

        try (Session session = sessionFactory.openSession()) {
            municipality = session.createQuery("SELECT m FROM Municipality m WHERE m.name = :name", Municipality.class)
                    .setParameter("name", municipalityName)
                    .getSingleResult();
        }

        return municipality;
    }
}
