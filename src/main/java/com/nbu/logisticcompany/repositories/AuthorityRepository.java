package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.Authority;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class AuthorityRepository  {
    private SessionFactory sessionFactory;

    @Autowired
    public AuthorityRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void create(Authority authority) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(authority);
            session.getTransaction().commit();
        }
    }
}
