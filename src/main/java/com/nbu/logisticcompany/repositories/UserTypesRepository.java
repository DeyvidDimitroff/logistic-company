package com.nbu.logisticcompany.repositories;

import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.models.UserTypes;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserTypesRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserTypesRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public List<UserTypes> getAll() {
        List<UserTypes> userTypes;

        try (Session session = sessionFactory.openSession()) {
            userTypes = session.createQuery("SELECT ut FROM UserTypes ut", UserTypes.class).list();
        }

        return userTypes;
    }

    public UserTypes getById(int id) {
        UserTypes userTypes;

        try (Session session = sessionFactory.openSession()) {
            userTypes = session.get(UserTypes.class, id);
        }

        return userTypes;
    }
}
