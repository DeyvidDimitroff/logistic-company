package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.OfficeDto;
import com.nbu.logisticcompany.dtos.ShipmentCreationDto;
import com.nbu.logisticcompany.dtos.ShipmentDto;
import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.models.Shipment;
import com.nbu.logisticcompany.models.UserShipment;
import com.nbu.logisticcompany.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipmentService {
    private ShipmentRepository shipmentRepository;
    private UserShipmentRepository userShipmentRepository;
    private UserRepository userRepository;
    private UserTypesRepository userTypesRepository;
    private OfficeRepository officeRepository;

    @Autowired
    public ShipmentService(ShipmentRepository shipmentRepository, UserShipmentRepository userShipmentRepository,
                           UserRepository userRepository, UserTypesRepository userTypesRepository,
                           OfficeRepository officeRepository) {
        this.shipmentRepository = shipmentRepository;
        this.userShipmentRepository = userShipmentRepository;
        this.userRepository = userRepository;
        this.userTypesRepository = userTypesRepository;
        this.officeRepository = officeRepository;
    }

    public List<ShipmentDto> getAll() {
        return ShipmentDto.convertList(shipmentRepository.getAll());
    }

    public ShipmentDto getById(int id) {
        return ShipmentDto.convert(shipmentRepository.getById(id));
    }

    public void create(ShipmentCreationDto shipment) {
        Shipment newShipment = new Shipment();
        newShipment.setIsReceived(false);
        newShipment.setIsSentToAddress(shipment.getIsSentToAddress().equals("address"));
        OfficeDto o = OfficeDto.convert(officeRepository.getById(shipment.getOffice()));
        if (shipment.getIsSentToAddress().equals("address")) {
            newShipment.setShippingAddress(shipment.getShippingAddress());
        } else {
            newShipment.setShippingAddress(o.getAddress() + ", " + o.getCity());
        }
        newShipment.setPrice(shipment.getPrice());
        newShipment.setWeight(shipment.getWeight());
        shipmentRepository.create(newShipment);

        UserShipment userShipmentRecipient = new UserShipment();
        userShipmentRecipient.setShipment(newShipment);
        userShipmentRecipient.setUser(userRepository.getUserByUserName(shipment.getRecipientName()));
        userShipmentRecipient.setUserTypes(userTypesRepository.getById(2));
        userShipmentRepository.create(userShipmentRecipient);

        UserShipment userShipmentSender = new UserShipment();
        userShipmentSender.setShipment(newShipment);
        userShipmentSender.setUser(userRepository.getUserByUserName(shipment.getSenderName()));
        userShipmentSender.setUserTypes(userTypesRepository.getById(1));
        userShipmentRepository.create(userShipmentSender);

    }
}
