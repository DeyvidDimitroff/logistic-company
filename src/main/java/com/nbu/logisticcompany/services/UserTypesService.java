package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.UserTypesDto;
import com.nbu.logisticcompany.repositories.UserTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserTypesService {
    private UserTypesRepository userTypesRepository;

    @Autowired
    public UserTypesService(UserTypesRepository userTypesRepository) {
        this.userTypesRepository = userTypesRepository;
    }

    public List<UserTypesDto> getAll() {
        return UserTypesDto.convertList(userTypesRepository.getAll());
    }
}
