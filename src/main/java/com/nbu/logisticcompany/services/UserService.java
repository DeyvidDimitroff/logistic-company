package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.UserCreationDTO;
import com.nbu.logisticcompany.dtos.UserDto;
import com.nbu.logisticcompany.exceptions.InvalidUserInputException;
import com.nbu.logisticcompany.models.Authority;
import com.nbu.logisticcompany.models.User;
//import com.nbu.logisticcompany.repositories.UserRoleRepository;
import com.nbu.logisticcompany.models.UserDetails;
import com.nbu.logisticcompany.repositories.AuthorityRepository;
import com.nbu.logisticcompany.repositories.UserDetailsRepository;
import com.nbu.logisticcompany.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private PasswordEncoder encoder;
    private UserRepository userRepository;
    private UserDetailsRepository userDetailsRepository;
    private AuthorityRepository authorityRepository;

    @Autowired
    public UserService(UserRepository userRepository, AuthorityRepository authorityRepository, PasswordEncoder encoder,
                       UserDetailsRepository userDetailsRepository) {
        this.userRepository = userRepository;
        this.encoder = encoder;
        this.authorityRepository = authorityRepository;
        this.userDetailsRepository = userDetailsRepository;
    }

    public List<User> getAll() {
        return userRepository.getAll();
    }

    public void createClient(UserCreationDTO user) {
        User dbUser = userRepository.getUserByUserName(user.getEmail());
        if (dbUser != null) {
            throw new InvalidUserInputException("Username with that name already exist");
        }
        User userToBeCreated = new User();
        String password = encoder.encode(user.getPassword());
        userToBeCreated.setPassword(password);
        UserDetails userDetails = new UserDetails();
        userDetails.setPhoneNumber(user.getPhoneNumber());
        userDetailsRepository.create(userDetails);
        userToBeCreated.setUserDetails(userDetails);
        userToBeCreated.setName(user.getName());
        userToBeCreated.setEmail(user.getEmail());
        userToBeCreated.setEnabled(true);
        userRepository.create(userToBeCreated);
        Authority authority = new Authority();
        authority.setEmail(user.getEmail());
        authority.setAuthority("ROLE_USER");
        authorityRepository.create(authority);
    }

    public void createEmployee(UserCreationDTO user) {
        User dbUser = userRepository.getUserByUserName(user.getEmail());
        if (dbUser != null) {
            throw new InvalidUserInputException("Username with that name already exist");
        }
        User userToBeCreated = new User();
        String password = encoder.encode(user.getPassword());
        userToBeCreated.setPassword(password);
        UserDetails userDetails = new UserDetails();
        userDetails.setPhoneNumber(user.getPhoneNumber());
        userDetails.setSalary(user.getSalary());
        userDetailsRepository.create(userDetails);
        userToBeCreated.setUserDetails(userDetails);
        userToBeCreated.setName(user.getName());
        userToBeCreated.setEmail(user.getEmail());
        userToBeCreated.setEnabled(true);
        userRepository.create(userToBeCreated);
        Authority authority = new Authority();
        authority.setEmail(user.getEmail());
        authority.setAuthority("ROLE_USER");
        authorityRepository.create(authority);
    }

    public User getById(int id) {
        User user = userRepository.getById(id);
        return user;
    }

    public User getUserByUserName(String username) {
        return userRepository.getUserByUserName(username);
    }

    public void enableOrDisableUser(Integer userId) {
        User user = userRepository.getById(userId);
        if (user.isEnabled()) {
            user.setEnabled(false);
        } else {
            user.setEnabled(true);
        }
        userRepository.update(user);
    }

    public void update(UserDto user) {
        User dbUser = userRepository.getById(user.getId());
        userRepository.update(dbUser);
    }
}
