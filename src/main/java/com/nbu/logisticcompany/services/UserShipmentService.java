package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.ShipmentDto;
import com.nbu.logisticcompany.dtos.UserDto;
import com.nbu.logisticcompany.dtos.UserShipmentDto;
import com.nbu.logisticcompany.models.UserShipment;
import com.nbu.logisticcompany.repositories.ShipmentRepository;
import com.nbu.logisticcompany.repositories.UserRepository;
import com.nbu.logisticcompany.repositories.UserShipmentRepository;
import com.nbu.logisticcompany.repositories.UserTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserShipmentService {
    private UserShipmentRepository userShipmentRepository;
    private ShipmentRepository shipmentRepository;
    private UserRepository userRepository;
    private UserTypesRepository userTypesRepository;

    @Autowired
    public UserShipmentService(UserShipmentRepository userShipmentRepository, ShipmentRepository shipmentRepository,
                               UserRepository userRepository, UserTypesRepository userTypesRepository) {
        this.userShipmentRepository = userShipmentRepository;
        this.shipmentRepository = shipmentRepository;
        this.userRepository = userRepository;
        this.userTypesRepository = userTypesRepository;
    }

    public List<UserShipmentDto> getAll() {
        return UserShipmentDto.convertList(userShipmentRepository.getAll());
    }

    public List<UserDto> getAllEmployees() {
        return UserDto.convertList(userShipmentRepository.getAllEmployees());
    }

    public List<UserDto> getAllClients() {
        return UserDto.convertList(userShipmentRepository.getAllClients());
    }

    public List<UserDto> getAllSenders() {
        return UserDto.convertList(userShipmentRepository.getAllSenders());
    }

    public List<UserDto> getAllReceivers() {
        return UserDto.convertList(userShipmentRepository.getAllReceivers());
    }

    public List<ShipmentDto> getShipmentsForUser(String email) {
        return ShipmentDto.convertList(userShipmentRepository.getShipmentsByUser(email));
    }

    public void approve(Integer shipmentId, String userEmail) {
        UserShipment userShipment = new UserShipment();

        userShipment.setUser(userRepository.getUserByUserName(userEmail));
        userShipment.setShipment(shipmentRepository.getById(shipmentId));
        userShipment.setUserTypes(userTypesRepository.getById(3));
        userShipmentRepository.create(userShipment);
    }

    public void deliver(Integer shipmentId, String userEmail) {
        UserShipment userShipment = new UserShipment();

        userShipment.setUser(userRepository.getUserByUserName(userEmail));
        userShipment.setShipment(shipmentRepository.getById(shipmentId));
        userShipment.setUserTypes(userTypesRepository.getById(4));
        userShipmentRepository.create(userShipment);
    }
}
