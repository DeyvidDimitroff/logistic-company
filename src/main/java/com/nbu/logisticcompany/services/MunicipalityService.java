package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.MunicipalityDto;
import com.nbu.logisticcompany.models.Municipality;
import com.nbu.logisticcompany.repositories.MunicipalityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MunicipalityService {
    private MunicipalityRepository municipalityRepository;

    @Autowired
    public MunicipalityService(MunicipalityRepository municipalityRepository) {
        this.municipalityRepository = municipalityRepository;
    }

    public List<MunicipalityDto> getAll() {
        List<Municipality> municipalities = municipalityRepository.getAll();
        ArrayList<MunicipalityDto> municipalityDtos = new ArrayList<>();
        for (Municipality m :
                municipalities) {
            municipalityDtos.add(MunicipalityDto.fromObject(m));
        }
        return municipalityDtos;
    }

}
