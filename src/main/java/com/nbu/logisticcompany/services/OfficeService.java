package com.nbu.logisticcompany.services;

import com.nbu.logisticcompany.dtos.OfficeDto;
import com.nbu.logisticcompany.dtos.ShipmentDto;
import com.nbu.logisticcompany.models.Office;
import com.nbu.logisticcompany.repositories.MunicipalityRepository;
import com.nbu.logisticcompany.repositories.OfficeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OfficeService {
    private OfficeRepository officeRepository;
    private MunicipalityRepository municipalityRepository;

    @Autowired
    public OfficeService(OfficeRepository officeRepository, MunicipalityRepository municipalityRepository) {
        this.officeRepository = officeRepository;
        this.municipalityRepository = municipalityRepository;
    }

    public List<OfficeDto> getAll() {
        return OfficeDto.convertList(officeRepository.getAll());
    }

    public void create (OfficeDto officeDto) {
        Office office = new Office();
        office.setCity(municipalityRepository.getMunicipalityByName(officeDto.getCity()));
        office.setStreetAddress(officeDto.getAddress());
        office.setWorkingTime(officeDto.getWorkingTime());

        officeRepository.create(office);
    }

    public void delete (Integer officeId) {
        Office office = officeRepository.getById(officeId);

        officeRepository.delete(office);
    }
}
